FROM golang:alpine3.12 as build
WORKDIR /go/src/minica
COPY main.go .
RUN go get && \
    CGO_ENABLED=0 go build -o /go/bin/minica

FROM scratch as run
COPY --from=build /go/bin/minica /minica
VOLUME /tmp
ENTRYPOINT ["/minica"]
